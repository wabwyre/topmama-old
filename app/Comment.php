<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['comment', 'ip_address', 'book_id'];

    public function scopeIdDescending($query){
        return $query->orderBy('id','DESC');
    }  
}