<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    protected $fillable = ['name', 'gender', 'culture', 'born', 'died', 'titles', 'aliases', 'playedBy'];

    public function scopeNameAscending($query){
        return $query->orderBy('name','ASC');
    }  
}
