<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use App\Comment;
class CommentController extends Controller
{

        /**
 * @OA\Post(
 *     path="/api/create-comment",
 *     @OA\Response(response="200", description="Add  Comments.")
 * )
 */
  public function create(Request $request){
    $rules = [
        'comment' => 'required',
        'book_id' => 'required',
     ];

    $customMessages = [
        'required' => 'Please fill attribute :attribute'
   ];
   $this->validate($request, $rules, $customMessages);
   try {
    $clientIP = request()->getClientIp();
    $comment = $request->input('comment');
    $book_id = $request->input('book_id');

    $save = Comment::create([
        'comment'=> $comment,
        'book_id'=> $book_id,
        'ip_address'=> $clientIP
    ]);

    $res['status'] = true;
    $res['message'] = 'Comment added successfully!';
    return response($res, 200);

    } catch (\Illuminate\Database\QueryException $ex) {
        $res['status'] = false;
        $res['message'] = $ex->getMessage();
        return response($res, 500);
    }

  }


       /**
 * @OA\Get(
 *     path="/api/get-comments",
 *     @OA\Response(response="200", description="Display a listing of Comments.")
 * )
 */
  public function get_comments(){
    $Comment = Comment::idDescending()->paginate(10);
    if ($Comment) {
          $res['status'] = true;
          $res['message'] = $Comment;

          return response($res);
    }else{
      $res['status'] = false;
      $res['message'] = 'Cannot find Comments!';

      return response($res);
    }
}

}