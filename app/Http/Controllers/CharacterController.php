<?php

namespace App\Http\Controllers;

use App\Character;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CharacterController extends Controller {
    //save character data from external endpoint 
        /**
 * @OA\Get(
 *     path="/api/fetch-characters",
 *     @OA\Response(response="200", description="Save Characters from https://anapioficeandfire.com/api/characters.")
 * )
 */
    public function saveApiData(){
         try 
         {
            $totalItems = 2140;
            $itemsPerPage = 50;
            $limit = ceil($totalItems / $itemsPerPage);
            $i = 1;
            do{
                $i++;
               
                 $response = Http::get('https://anapioficeandfire.com/api/characters',['page'=> $i, 'pageSize'=>'50']);
                 $data= json_decode($response->getBody(), true);
                   foreach($data as $key=>$value){ 
                    $Character = new Character;
                    $Character->name = $value['name'];
                    $Character->gender = $value['gender'];
                    $Character->culture = $value['culture'];
                    $Character->born = $value['born'];
                    $Character->died = $value['died'];
                    $Character->titles = json_encode($value['titles']);
                    $Character->aliases = json_encode($value['aliases']);
                    $Character->playedBy = json_encode($value['playedBy']);
                    $Character->save();
                }
            }
            while($i <= $limit);

                //return successful response
                $res['status'] = true;
                $res['message'] = 'Character records saved!';
                return response($res, 200);
                //return response()->json(['message' => 'Character records saved'], 201);
                } 
                catch (\Illuminate\Database\QueryException $ex) 
                {
                //return error message
                $res['status'] = false;
                $res['message'] = $ex->getMessage();
                return response($res, 500);
                //return response()->json(['message' => 'Save from Character api failed!'], 409);
                }
    
      } 
      
       /**
 * @OA\Get(
 *     path="/api/get-characters",
 *     @OA\Response(response="200", description="Display a listing of Characters.")
 * )
 */
      public function get_characters(){
        $Character = Character::nameAscending()->paginate(10);
        if ($Character) {
              $res['status'] = true;
              $res['message'] = $Character;
 
              return response($res);
        }else{
          $res['status'] = false;
          $res['message'] = 'Cannot find Characters!';
 
          return response($res);
        }
    }

         /**
 * @OA\Get(
 *     path="/api/search-characters",
 *     @OA\Response(response="200", description="search character based on gender")
 * )
 */
    public function search(Request $request)
    {
        $search = $request->search;
        try {
            $data = DB::table("characters")->where("gender", $search)->get();
            $count = DB::table("characters")->where("gender", $search)->count();
            $res['success'] = true;
            $res['data'] = $data;
            $res['count'] = $count;
            $res['message'] = "get characters success";
            return response($res, 200);
        } catch (\Illuminate\Database\QueryException $ex) {
            $res['success'] = false;
            $res['message'] = $ex->getMessage();
            return response($res, 500);
        }
    }

}