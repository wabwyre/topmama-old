<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Support\Facades\Http;

class BookController extends Controller {

  
     /**
 * @OA\Get(
 *     path="/api/fetch-books",
 *     @OA\Response(response="200", description="Save Books from https://www.anapioficeandfire.com/api/books.")
 * )
 */
    public function saveApiData(){
       try 
       {
        $response = Http::get('https://www.anapioficeandfire.com/api/books',['page'=> '1', 'pageSize'=>'20']);
       
        $arrays = $response->body();
       $data= json_decode($response->getBody(), true);

         //return $data;
        foreach($data as $key=>$value){
            $Book = new Book;
            $Book->name = $value['name'];
            $Book->isbn = $value['isbn'];
            $Book->authors = json_encode($value['authors']);
            $Book->pages = $value['numberOfPages'];
            $Book->publisher = $value['publisher'];
            $Book->country = $value['country'];
            $Book->media_type = $value['mediaType'];
            $Book->released = $value['released'];
            $Book->save();
        }
    
         //return successful response
         $res['status'] = true;
         $res['message'] = 'Books records saved!';
         return response($res, 200);
        //  return response()->json(['message' => 'Books records saved'], 201);
       } 
       catch (\Illuminate\Database\QueryException $ex) 
       {
         //return error message
         $res['status'] = false;
         $res['message'] = $ex->getMessage();
         return response($res, 500);
         //return response()->json(['message' => 'Save from books api failed!'], 409);
       }


    }

    /**
 * @OA\Get(
 *     path="/api/get-books",
 *     @OA\Response(response="200", description="Display a listing of Books.")
 * )
 */

    public function get_books(){
        $books = Book::releasedAscending()->paginate(10);
        if ($books) {
              $res['status'] = true;
              $res['message'] = $books;
 
              return response($res);
        }else{
          $res['status'] = false;
          $res['message'] = 'Cannot find Books!';
 
          return response($res);
        }
    }
}