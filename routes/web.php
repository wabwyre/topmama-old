<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/key', function() {
    return \Illuminate\Support\Str::random(32);
});

$router->group(['prefix' => 'api'], function () use ($router) 
{
    $router->post('login', 'LoginController@login');
    $router->post('register', 'UserController@register');
    $router->get('user', ['middleware' => 'auth', 'uses' =>  'UserController@get_user']);
    $router->get('fetch-books',  ['middleware' => 'auth', 'uses' =>  'BookController@saveApiData']);
    $router->get('get-books',  ['middleware' => 'auth', 'uses' =>  'BookController@get_books']);
    $router->get('fetch-characters',  ['middleware' => 'auth', 'uses' =>  'CharacterController@saveApiData']);
    $router->get('get-characters',  ['middleware' => 'auth', 'uses' =>  'CharacterController@get_characters']);
    $router->get('search-characters',  ['middleware' => 'auth', 'uses' =>  'CharacterController@search']);
    $router->get('get-comments',  ['middleware' => 'auth', 'uses' =>  'CommentController@get_comments']);
    $router->post('create-comment',  ['middleware' => 'auth', 'uses' =>  'CommentController@create']);
});
